# TRASE DATA SCIENTIST EXERCISE
# Part 2: Analysis and visualization

Analysis and visualization
Short description of the result:

Beef exports from Pará are increasingly less destined for Venezuela. Impacted by the drastic reduction of Venezuelan importations and investments, Brazil has been exporting less and less beef to the neighboring country. For a long time the meat embarked in the ports of the state of Pará had an almost certain destination for Venezuela, but more recently this scenario has changed. Since 2015 exports have moved to a more distant destination, going with more frequency to Egypt, Russia and Hong Kong. The number of municipalities that supplies beef for these countries is not large; only 8 municipalities participate directly in this chain. In the last three years, the beef that goes to these countries mainly departs from the municipalities of Abaetetuba, Mojú and Acará, being shipped in the ports of Barcarena and Belém. Even though there are few municipalities directly involved, these municipalities received animals from lots of more municipalities during the cattle ranching process, making it difficult to monitor the chain. Additionally, the diversification of the market across the time represents a challenge for sustainable commitments between countries and suppliers, reinforcing the need for a global commitment to the sustainability of the commodities chains.

To access the interactive visualization: http://rpubs.com/vivian_rbr/sei_ds_exercisepart2
To access the gitlab repository: https://gitlab.com/vivian_rbr/sei_exercisepart2


### Prerequisites
libraries required

```
install.packages("flexdashboard")
install.packages("dplyr")
install.packages("highcharter")
install.packages("purrr")
install.packages("treemap")
install.packages("viridisLite")
install.packages("RColorBrewer")
install.packages("networkD3")
```
## Version

* First version from February 17, 2019

## Authors

* **Vivian Ribeiro** - *Initial work* - (https://gitlab.com/vivian_rbr)
